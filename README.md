# IMPORTANT NOTE
I wrote this code when i was kid, i learned python then wrote my first application, Muas. There is alot of bugs. Maybe rewrite it in Clojure or Python again.

# Muas
Version Alpha Assistant

# How to use:

go to same directory with cmd and say: python mainf.py

# Commands

 Answers Classic Questions Like "Hello!"

 Open any program like: open Firefox

 Finding anywhere in the world with: where is (To find)

 News with: what's new & news

 Clearing chat with: clear
 
 Locking Computer With: lock my computer
 
 Recycle bin clear: Clears Recycle bin.
 
 Info with: info

# Music:
  
  Set your music path from settings.json and say: music play (Music from the directory) WARNING: When you setting the path dont   forgot add / to end.

Pausing with: music pause

Unpausing with: music continue

Stopping with: music stop

# 5. Search:

Google Search: search for (To search)

Youtube Search: search video for (To search)

Book Search: search book for (To search)

WolframAlpha Search: what is & who is (To Search)

# 9. Keyboard:

Start typing with voice: keyboard

and Say anything to write.

Stop typing with: stop

# 11. Mail:

Send mail with: send (To send)        (You need to write his email to users.json)

Say space to press space

Say "send it" to send the message and say yes

