import webbrowser
from pygame import mixer
from datetime import datetime
import wikipedia
from bs4 import BeautifulSoup
import requests
import pyautogui as pag
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
### Search ###
def g (this):
    engine = "https://www.google.com/?#q="
    webbrowser.open(engine + this, new=2)
def wa (this):
    engine = "https://www.wolframalpha.com/input/?i="
    webbrowser.open(engine + this, new=2)
def wp (this):
    print(wikipedia.page(this).content)
def y (this):
    engine = "https://www.youtube.com/results?search_query="
    webbrowser.open(engine + this, new=2)
def gb (this):
    engine = "https://www.google.com/search?tbm=bks&q="
    webbrowser.open(engine + this, new=2)
def gm (this):
    engine = "https://www.google.com.tr/maps/place/"
    webbrowser.open(engine + this, new=2)
def get (this):
    webbrowser.open(this, new=2)
# Music
def smusic(path):
    mixer.init()
    mixer.music.load(path)
def pmusic():
    mixer.music.play()
def psmusic():
    mixer.music.pause()
def upmusic():
    mixer.music.unpause()
def stmusic():
    mixer.music.stop()
def wms():
    time = datetime.now()
    if time.hour < 4:
        quit("Sleep well.")
    elif time.hour < 9:
        print("Good morning.")
        return "Good morning."
    elif time.hour < 13:
        print("Hello.")
        return "Hello."
    elif time.hour < 17:
        print ("Good afternoon.")
        return "Good afternoon."
    elif time.hour < 23:
        print ("Good evenings.")
        return "Good evenings."
    else:
        print("ok.")
        return "ok."
# News
def news():
    url = "https://news.google.com/news/?ned=us&gl=US&hl=en"
    resp = requests.get(url)
    cont = resp.content
    soup = BeautifulSoup(cont,"html.parser")
    for new in soup.find_all("a",{"class":"nuEeue hzdq5d ME7ew"})[:9]:
        print("λ "+new.text)
        print("λ "+new.get("href"))
        print("-"*len(new.text))
# Write
def write(this):
    if this[:5] in "enter":
        pag.keyDown('enter')
        pag.keyUp('enter')
    elif 'space' == this:
        pag.typewrite(' ')
    elif this[:9] in "stop type":
        return 'stop'
    else:
        pag.typewrite(this)
# Mailing
def mail(e,p,t,m):
    mail = MIMEMultipart()
    msg = MIMEText(m,"plain")
    mail["From"] = e
    mail["To"] = t
    mail["Subject"] = "A Mail From "+e
    mail.attach(msg)
    meil = smtplib.SMTP("smtp.gmail.com",587)
    meil.ehlo()
    meil.starttls()
    meil.login(e,p)
    meil.sendmail(mail["From"],mail["To"],mail.as_string())
