class as_:
    def __init__(self):
        import pyttsx3
        import funcs
        from os import system
        import json
        import speech_recognition as sr
        from time import sleep
        from datetime import datetime
        self.wait = lambda x: sleep(x)
        self.sr = sr
        self.f = funcs
        self.json = json
        self.sys = lambda x: system(x)
        self.e = pyttsx3.init()
        self.now = datetime.now()
        self.datetiem = datetime.strftime(self.now, '%A')+", "+datetime.strftime(self.now, '%B')
        self.e.setProperty('voice', self.e.getProperty('voices')[0].id)
    def say(self,text):
        self.e.say(text)
        self.e.runAndWait()
        self.wait(0.449)
        return text
    def phrase(self):
        r = self.sr.Recognizer()
        with self.sr.Microphone() as source:
            audio = r.listen(source)
        try:
            print(r.recognize_google(audio))
            return r.recognize_google(audio)
        except self.sr.UnknownValueError:
            return "U"
        except self.sr.RequestError as e:
            quit("No Connection; {0}".format(e))
    def anw(self,q):
        if q == "U":
            pass
        elif q == "hello":
            print("λ: Hello.")
            self.say("Hello.")
            self.sys("cls")
        elif q == "how are you":
            print("λ: Im fine.")
            self.say("Im fine.")
            self.sys("cls")
        elif q == "who are you":
            print("λ: Im Mu.")
            self.say("Im Mu.")
            self.sys("cls")
        elif q[:6] == "system":
            self.sys(q[7:])
            self.wait(1)
            self.sys("cls")
        elif q == "what can you do":
            print("I can do about everything needs to be in a assistant.")
            self.say("I can do about everything need in a assistant.")
            self.sys("cls")
        elif q in ['goodbye','terminate','exit','bye']:
            print("λ: Goodbye. See you next time.")
            self.say("Goodbye. See you next time.")
            raise SystemExit(0)
        elif q[:4] == "open":
            print("λ: Opening "+q[5:])
            self.say("Opening "+q[5:])
            self.sys("start "+q[5:])
        elif q[:5] == "music":
            if q[6:10] == "play":
                with open("config/settings.json") as data:
                    path = self.json.load(data)
                    self.f.smusic(path["music_path"]+q[11:]+".mp3")
                    print("λ: Playing "+q[11:])
                    self.say("Playing "+q[11:])
                    self.f.pmusic()
            elif q[6:11] == "pause":
                self.f.psmusic()
                print("λ: Music paused.")
                self.say("Music paused.")
            elif q[6:14] == "continue":
                print("λ: Contiuneing to music.")
                self.say("Contiuneing to music.")
                self.f.upmusic()
            elif q[6:10] == "stop":
                self.f.stmusic()
                print("λ: Music stopped.")
                self.say("Music stopped.")
            else:
                print("λ: " + q + ": unknown command.")
                self.wait(0.6752)
                self.sys("cls")
        elif q == "what are you doing":
            print("Speaking with you.")
            self.say("Speaking with you.")
            self.sys("cls")
        elif q[:16] in "lock my computer":
            print("λ: Locking the computer.")
            self.say("Locking the computer.")
            self.sys("rundll32.exe user32.dll,LockWorkStation")
        elif q[:6] in "search":
            if q[7:10] in "for":
                print("λ: Looking for "+q[11:])
                self.say("Looking for "+q[11:])
                self.f.g(q[11:])
                self.sys("cls")
            elif q[7:16] in "video for":
                print("λ: Looking video's for " + q[17:])
                self.say("Looking video's for " + q[17:])
                self.f.y(q[17:])
                self.sys("cls")
            elif q[7:15] in "book for":
                print("λ: Looking book's for " + q[16:])
                self.say("Looking book's for " + q[16:])
                self.f.gb(q[16:])
                self.sys("cls")
        elif q[:8] in "where is":
            print("λ Finding " + q[9:])
            self.say("Finding " + q[9:])
            self.f.gm(q[9:])
            self.sys("cls")
        elif q[:6] in "who is" or q[:7] in "what is" and q[8:13] not in "today":
            if q[:6] in "who is":
                print("λ: Searching " + q[7:])
                self.say("Searching " + q[7:])
                self.f.wa(q[7:])
                self.sys("cls")
            else:
                print("λ: Searching " + q[8:])
                self.say("Searching " + q[8:])
                self.f.wa(q[8:])
                self.sys("cls")
        elif q[:13] in "what is today" or q[:12] in "what's today":
            print("λ: Today is",self.datetiem)
            self.say("Today is "+self.datetiem)
            self.sys("cls")
        elif q[:10] in "what's new":
            self.f.news()
        elif q[:5] in "clear":
            print("λ: Clearing chat.")
            self.say("Clearing chat.")
            self.sys("cls")
        elif q[:3] in "0xM":
            print("λ: Its my creators nick.")
            self.say("Its my creators nick.")
            self.sys("cls")
        elif q[:4] in "keyboard":
            print("Active")
            while 1:
                twrite = self.phrase()
                if twrite == 'stop':
                    print("λ: Stopped typing.")
                    self.say("Stopped typing.")
                    break
                self.f.write(twrite)
        elif q == "recycle bin clear":
            print("λ: Clearing recycle bin.")
            self.say("Clearing recycle bin.")
            self.sys("rd /s /q %systemdrive%\$RECYCLE.BIN")
        elif q[:4] in "send" and not q[5:] == "it":
            text = ''
            print("λ: Mail text:")
            while 1:
                txt = self.phrase()
                if txt == "send it":
                    break
                elif txt[:5] in "space":
                    text += " "
                else:
                    text += txt
            print("λ: "+text+"; To "+q[5:])
            yorn = self.phrase()
            if yorn == "yes":
                with open("config/account.json") as f:
                    mw = self.json.load(f)
                with open("config/users.json") as f:
                    user = self.json.load(f)
                try:
                    self.f.mail(mw["email"],mw["passw"],user[q[5:]],text)
                except KeyError:
                    print("λ: No one named; "+q[5:])
                    self.say("No one named; "+q[5:])
                except:
                    print("λ: Wrong Username or Password.")
                    self.say("Wrong Username or Password.")
                else:
                    print("λ: Message sended succesfully")
                    self.say("Message sended succesfully")
            else:
                pass
            self.sys("cls")
        elif q == "info":
            print("""
 ________Mu________   
| creator:Muhammed |
| version:Alpha 1.0|
 ------------------        
            """)
        else:
            print("λ: "+q+": unknown command.")
            self.wait(0.6752)
            self.sys("cls")
